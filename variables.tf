# Manditory variables for all modules
variable "huit_assetid" {
  description = "HUIT Asset ID as assigned in Slash"
}

variable "product" {
  description = "HUIT Product ID as assigned in Slash"
}

variable "environment" {
  description = "Application specific environment"
}

variable "environment_map" {
  description = "Application specific environment mapping that tags will be populated with"
  type    = "map"
  default = {
    dev   = "Development"
    test  = "Testing"
    stage = "Staging"
    prod  = "Production"
  }
}

# Required Data Providers
data "aws_availability_zones" "available" {}

variable "app_scope" {
  description = "Application scope. i.e. app, node, master, slave, etc..."
}

variable "aws_ami_id" {
  description = "The AMI ID to be used in the Launch Configuration"
}

variable "aws_instance_type" {
  description = "The instance type to be used in the Launch Configuration"
}

variable "aws_iam_instance_profile" {
  description = "The IAM instance profile to be used in the Launch Configuration"
}

variable "aws_key_name" {
  description = "The SSH Key name to be used in the Launch Configuration"
}

variable "aws_security_groups" {
  description = "A comma delimited string of security groups the instances use"
}

variable "template_file" {
  description = "The path to an HCL template file to use for instance userdata"
  default     = "templates/template_file.sh.tpl"
}

variable "aws_asg_number_of_instances" {
  description = "The number of instances we want in the ASG"
  // We use this to populate the following ASG settings
  // - max_size
  // - desired_capacity
}

variable "aws_asg_minimum_number_of_instances" {
  description = "The minimum number of instances the ASG should maintain"
  default = 1
  // Defaults to 1
  // Can be set to 0 if you never want the ASG to replace failed instances
}

variable "aws_asg_health_check_grace_period" {
  description = "Number of seconds for a health check to time out"
  default = 300
}

variable "aws_asg_health_check_type" {
  description = "The type of health check the AutoScaling Group uses to evaluate instance health"
  default = "ELB"
  //Types available are:
  // - ELB
  // - EC2
  // * http://docs.aws.amazon.com/cli/latest/reference/autoscaling/create-auto-scaling-group.html#options
}

variable "aws_load_balancer_names" {
  description = "A comma seperated list string of ELB names the ASG should associate instances with"
}

variable "aws_instance_subnet_ids" {
  description = "A comma separated string of the VPC subnet IDs to use for the AutoScaling Group"
}

variable "user_data" {
  description = "A data resource reference to a user_data template"
}
