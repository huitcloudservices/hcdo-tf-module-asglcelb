# AWS Launch Configuration Creation
resource "aws_launch_configuration" "launch_config" {
  name_prefix          = "${lower(var.product)}-${lower(var.environment)}-${lower(var.app_scope)}-lc-"
  image_id             = "${var.aws_ami_id}"
  instance_type        = "${var.aws_instance_type}"
  iam_instance_profile = "${var.aws_iam_instance_profile}"
  key_name             = "${var.aws_key_name}"
  security_groups      = ["${split(",", var.aws_security_groups)}"]
  user_data            = "${var.user_data}"
  lifecycle {
    create_before_destroy = true
  }
}

# AWS AutoScaling Group Creation
resource "aws_autoscaling_group" "main_asg" {
  depends_on                = ["aws_launch_configuration.launch_config"]
  name                      = "${lower(var.product)}-${lower(var.environment)}-${lower(var.app_scope)}-asg"
  vpc_zone_identifier       = ["${split(",", var.aws_instance_subnet_ids)}"]
  launch_configuration      = "${aws_launch_configuration.launch_config.id}"
  max_size                  = "${var.aws_asg_number_of_instances}"
  min_size                  = "${var.aws_asg_minimum_number_of_instances}"
  desired_capacity          = "${var.aws_asg_number_of_instances}"
  health_check_grace_period = "${var.aws_asg_health_check_grace_period}"
  health_check_type         = "${var.aws_asg_health_check_type}"
  load_balancers            = ["${split(",", var.aws_load_balancer_names)}"]
  force_delete              = true

  tag {
    key                 = "Name"
    value               = "${lower(var.product)}-${lower(var.environment)}-${lower(var.app_scope)}-ec2asg"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "huit_assetid"
    value               = "${var.huit_assetid}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "product"
    value               = "${var.product}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "environment"
    value               = "${lookup(var.environment_map, lower(var.environment))}"
    propagate_at_launch = "true"
  }
}
